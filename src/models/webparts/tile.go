// web contains models for web entities
package webparts

// Tile is a wrapper for a card content on the home page
type Tile struct {
	Title  string
	Hero   string
	Body   string
	Footer string
}
